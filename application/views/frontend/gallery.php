<div class="container pad_50_15 gallery">
    <?php foreach($records as $r) { ?>
    <div class="gallery_item">
        <a href="<?= base_url(); ?>uploads/gallery/<?php echo $r->image ?>" data-sub-html="<h3><?= $r -> caption; ?></h3>">
            <div class="gallery_item_inner" style="background: url('<?= base_url(); ?>uploads/gallery/<?php echo $r->image ?>') no-repeat center; background-size: cover;"><img style="visibility: hidden" src="<?= base_url(); ?>uploads/gallery/<?php echo $r->image ?>"></div>
            <div class="overlay"></div>
        </a>
    </div>
    <?php } ?>
</div>

<script>
    jQuery(document).ready(function ($) {
        $('.gallery').lightGallery({
            download: false,
            thumbnail: true,
            fullScreen: false,
            selector: '.gallery_item a',
            animateThumb: true
        });
    });
</script>
