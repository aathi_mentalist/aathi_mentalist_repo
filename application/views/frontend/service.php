<div class="container pad_50_15">
    <h3 class="service_head">Our <strong>Services</strong></h3>
    <p class="service_main_det">Traiac extend its hands to service side too. We have a automation wing that engage in reducing human efforts and errors thereby focusing on optimized result using modern technological advancements. We also have a calibration consultancy which provide  assistance for  calibrating Primary and Secondary standard type meters including Flow. Our other service side include Research and Development plus shutdown works.</p>
    <hr class="custom">
    <div class="service_wrap">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3">
                <div class="service_box service_box_primary text-center">
                    <div class="ico"><i class="fa fa-cogs"></i><span class="ico_inner"></span></div>
                    <h4>Automation</h4>
                    <p>We Use
	SCADA , LABVIEW.
	Hardware and software design.</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
                <div class="service_box service_box_danger text-center">
                    <div class="ico"><i class="fa fa-cogs"></i><span class="ico_inner"></span></div>
                    <h4>3rd party calibration</h4>
                    <p>Physical standards, Electrotechnical
	Flow(Air, Water , Oil).</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
                <div class="service_box service_box_info text-center">
                    <div class="ico"><i class="fa fa-cogs"></i><span class="ico_inner"></span></div>
                    <h4>Project Works</h4>
                    <p>College Projects and provide research facilities.</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
                <div class="service_box service_box_black text-center">
                    <div class="ico"><i class="fa fa-cogs"></i><span class="ico_inner"></span></div>
                    <h4>Shut down works</h4>
                    <p>Servicing, Maintenance, Calibration, Installation.</p>
                </div>
            </div>
        </div>
    </div>
    <hr class="custom">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="feature_image">
                <img src="<?= base_url(); ?>images/gal_1.jpg" class="img-responsive">
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="feature_wrap">
                <h3>Our Salient <strong>Features</strong></h3>
                <ul>
                    <li>South India's biggest Automation lab Facility with international design and standards.</li>
                    <li> Advanced  centralised Scada Console with led hd screen interfaced with labview and Scada.</li>
                    <li>Well Experienced faculty from various organisations.</li>
                    <li>Online Classes Conducted By international experties.</li>
                    <li>Introduction to  linear instrumentation measurement techniques and calibration methodologies.</li>
                    <li>Industrial working experience through our advanced machinaries  interfaced with  PLC.</li>
                    <li>Visit  to various reputed industries inside and outside kerala.</li>
                    <li>100% quality assured and research oriented learning programme.</li>
                </ul>
            </div>
        </div>
    </div>
</div>