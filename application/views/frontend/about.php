<div class="container pad_50_15">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <img src="<?= base_url(); ?>images/about.jpg" class="img-responsive" alt="" />
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="about_us_det">
                <h3>About Traiac</h3>
                <p class="text-justify">Its an endeavor by a group of young engineers to extend the benefit of Automation to the lower level of the society by providing high quality products at a lower price by utilizing the technology in the conventional methods in an improved way, which is developed  and designed by continual experimentation in the field of Automation, results into more advanced and improved automated engineering in the industrial field, which would eventually lead to improved goods and services to the consumers and more importantly delivering compatible engineers for the extensive competitive field indeed...</p>
            </div>
        </div>
    </div>
</div>

<div class="why_us_wrapper">
    <div class="why_us_wrapper_inner pad_50">
        <div class="container text-center">
            <h3>WHY WE...?</h3>
            <p>Why should you select TRAIAC Automation and Research.  ... Reasons are lot... Our young and competitive engineering team are presenting their immense knowledge and varied experiences acquired by them working throughout the world for fulfilling your need... We are giving you an opportunity to use the same technology and appliances and thus to surpass the professional experience,  confidence and ability to get your dream job...</p>
            <div class="count_wrap text-center">
                <div class="count_item">
                    <div class="count_round">
                        <span class="count" data-max="2500" data-duration="1000">0</span> +
                    </div>
                    <h3>sqft Workshop</h3>
                </div>
                <div class="count_item">
                    <div class="count_round">
                        <span class="count" data-max="8" data-duration="800">0</span> +
                    </div>
                    <h3>PLC</h3>
                </div>
                <div class="count_item">
                    <div class="count_round">
                        <span class="count" data-max="5" data-duration="1800">0</span> +
                    </div>
                    <h3>SCADA</h3>
                </div>
                <div class="count_item">
                    <div class="count_round">
                        <span class="count" data-max="100" data-duration="4000">0</span> %
                    </div>
                    <h3>Quality</h3>
                </div>
            </div>
        </div>
    </div>
</div>