<footer>
    <div class="container text-center">
        <div class="footer_top">
            <a href="#" id="go_top">
                <div><i class="fa fa-caret-up"></i></div>
                <div>TOP</div>
            </a>
            <div class="footer_top_bg"></div>
        </div>
        <div class="footer_logo">
            <a href="index.html"><img src="<?= base_url(); ?>images/logo.png" alt="Traiac"></a>
        </div>
        <div class="brochure_lnk">
            <a href="#">Download Our Brochure</a>
        </div>
        <div class="footer_address">
            <img src="<?= base_url(); ?>images/map_marker.png" alt="">
            Savera Tower, Opp MV Hotel, Thrissur - Calicut Highway, Changaramkulam
        </div>
        <div class="footer_contact">
                <span>
                    <img src="<?= base_url(); ?>images/call_white.png" alt="">
                    <a href="tel:+917012776582">+91 7012 776 582</a>
                </span>
            <span>
                    <img src="<?= base_url(); ?>images/mail_white.png" alt="">
                    <a href="mailto:info@traiac.com">info@traiac.com</a>
                </span>
        </div>
        <div class="footer_social">
            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-google-plus"></i></a>
        </div>
        <div class="copy_wrap clearfix">
            <div class="copy">
                <script>document.write(new Date().getFullYear());</script>
                Copyright &copy; Traiac. All rights reserved.
            </div>
            <div class="power">
                Powered by <a href="http://bodhiinfo.com" target="_blank"><img src="<?= base_url(); ?>images/bodhi.png" alt="Bodhi"></a>
            </div>
        </div>
    </div>
</footer>
<div class="quick_contact">
    <div class="quick_head">Quick Contact</div>
    <div class="quick_form">
        <form action="<?php echo site_url(); ?>/Contact/quick_mail" method="post">
            <div class="form-group">
                <input type="text" name="name" placeholder="Your Name">
            </div>
            <div class="form-group">
                <input type="email" name="email" placeholder="Your Email">
            </div>
            <div class="form-group">
                <input type="text" name="subject" placeholder="Subject">
            </div>
            <div class="form-group">
                <textarea name="message" placeholder="Your Message"></textarea>
            </div>
            <div class="form-group">
                <input type="submit" name="submit" value="Submit Message">
            </div>
        </form>
    </div>
</div>
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="<?= base_url(); ?>js/lightgallery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
<script src="<?= base_url(); ?>js/lg-fullscreen.min.js"></script>
<script src="<?= base_url(); ?>js/lg-thumbnail.min.js"></script>
<script src="<?= base_url(); ?>js/jquery.bxslider.min.js"></script>
<script src="<?= base_url(); ?>js/app.js"></script>
<script src="<?= base_url(); ?>js/jscolor.js"></script>
<script src="<?= base_url(); ?>js/jscolor.min.js"></script>
<script>
    $(function () {
        $('.testimonial ul').bxSlider({
            auto: true,
            controls: false
        });
    });
</script>
<script>
    jQuery(document).ready(function ($) {
        $('.gallery').lightGallery({
            download: false,
            thumbnail: true,
            fullScreen: false,
            selector: '.gallery_item a',
            animateThumb: true
        });
    });
</script>
</body>
</html>