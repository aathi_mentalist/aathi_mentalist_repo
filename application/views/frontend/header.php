<!DOCTYPE HTML>
<html lang="">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Traiac</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/lightgallery.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/style.css">
</head>
<body>
<div class="top top_common">
    <div class="container clearfix">
        <div class="top_left">
                <span class="top_left_items">
                    <strong>Dail:</strong>
                </span>
            <span class="top_left_items">
                    <img src="<?= base_url(); ?>images/call.png" alt="">
                    <a href="tel:+917012776582">+917012776582</a>
                </span>
                <span class="top_left_items">
                    <strong>Mail To:</strong>
                </span>
            <span class="top_left_items">
                    <img src="<?= base_url(); ?>images/mail.png" alt="">
                    <a href="mailto:info@traiac.com">info@traiac.com</a>
                </span>
        </div>
        <div class="top_right">
            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-google-plus"></i></a>
        </div>
    </div>
</div>
<header class="common">
    <div class="container clearfix">
        <div class="logo">
            <a href="<?= base_url(); ?>index.php/Home/index"><img src="<?= base_url(); ?>images/logo.png" alt="Traiac" /></a>
        </div>
        <div class="nav_wrap">
            <ul class="clearfix">
                <li><a href="<?php echo site_url();?>/Home">Home</a></li>
                <li><a href="<?php echo site_url();?>/About">About Us</a></li>
                <li><a href="<?php echo site_url();?>/Service">Services</a></li>
                <li><a href="<?php echo site_url();?>/Questions">Students Portal</a></li>
                <li><a href="<?php echo site_url();?>/Gallery">Gallery</a></li>
                <li><a href="<?php echo site_url();?>/Contact">Contact Us</a></li>
            </ul>
        </div>
    </div>
    <span id="nav_trigger"><i class="fa fa-bars"></i></span>
</header>