<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="background-image:url(<?php echo base_url('img/aathi_new.jpg') ?>);">
    <section class="content-header" style="color:#E7E6E8">
        <h1>
        	 Video Links
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add New Video Links</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-10">
                                <form action="<?= site_url(); ?>/Video/edit" method="post">
                                 <?php foreach($results as $r){  ?>
                    			 <input type="hidden" name="editId" value="<?php echo $r['id'];?>">
                                    <div class="form-group">
                                        <label for="videoLink">Video Link</label><span class="text-danger">*</span>
                                        <input type="text" name="videoLink" id="videoLink" class="form-control" required value="<?= $r['video_link']; ?>" >
                                    </div>                                    
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                    <?php }?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

