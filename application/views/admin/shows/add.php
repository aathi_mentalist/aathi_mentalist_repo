<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="background-image:url(<?php echo base_url('img/aathi_new.jpg') ?>);">
    <section class="content-header" style="color:#E7E6E8">
        <h1>
            Add Shows
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add New Shows</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                <form action="<?= site_url(); ?>/Shows/add" method="post"  enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="title">Title</label><span class="text-danger">*</span>
                                        <input type="text" name="title" id="title" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Description</label><span class="text-danger">*</span>
                                        <!--<input type="text" name="description" id="description" class="form-control" required>-->
                                        <textarea name="description" id="description" class="form-control" required=""></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Show Image</label><span class="text-danger">*</span>
                                        <input type="file" name="image" id="image" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
