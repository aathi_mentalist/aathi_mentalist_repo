<?php
 class Questions_model extends CI_Model {
	protected $table='questions';
      function __construct() { 
         parent::__construct(); 
         $this->load->database();
      }
      
	public function getAllData(){
	    $this->db->select('*');
		$this->db->from('questions');
		$this->db->order_by('questions.id','desc');
		//$this->db->limit($limit, $offset);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();
	}
	public function insertData($params){ 
		$ins		  =	$this->db->insert($this->table,$params);//echo $this->db->last_query();die;
		return $ins;
	}
	public function getUpdateData($params)
	{ 
		$this->db->select($params['fields']);
		$query	=	$this->db->get_where($this->table,$params['condition']);
		return $query->result_array();		
	}
	
	public function updateAction($params,$editId)
	{
		$condition=array('id'=>$editId);
	 	$this->db->where($condition);
		$up		=	$this->db->update($this->table,$params);	
		return $up;
	}	
	public function getFrontendData(){
	    $this->db->select('*');
		$this->db->from('questions');
		$this->db->where("status='replied'");
		$this->db->order_by('questions.id','desc');
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();
	}

        //delete data
	public function deleteData($id) { 
    	if ($this->db->delete($this->table, "ID = ".$id)) { 
            return true; 
             } 
        }

      //for getting the questions content in home page.    
    public function getHomePageQuestionData($limit=3,$start=1){
	    $this->db->select('*');
		$this->db->from('questions');
		$this->db->where("status='replied'");
		$this->db->order_by('questions.id','desc');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();
	}

}