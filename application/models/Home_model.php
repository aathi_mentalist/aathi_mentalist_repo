<?php
 class Home_model extends CI_Model {
	protected $table='team';
	protected $table1='features';
      function __construct() { 
         parent::__construct(); 
         $this->load->database();
      }
      
	public function getAllData(){
	    $this->db->select('*');
		$this->db->from('team');
		$this->db->order_by('team.id','desc');
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();
	}
	public function getAllFeatureFrontData(){
	    $this->db->select('*');
		$this->db->from('features');
		$this->db->order_by('features.id','desc');
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();
	}
	public function getAllUpdateFrontData()
 	{
		$this->db->select('*');
		$this->db->from('latest_update');
		$this->db->order_by('latest_update.id','desc');
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result();
	}
	
	//taking all data from gallery for showing into front end
    public function getAllGalleryFrontData() {
		$query = $this->db->query("select * from gallery order by id DESC");
		//echo $this->db->last_query();
		return $query->result_array();
	}
}