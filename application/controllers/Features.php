<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Features extends CI_Controller {
	protected $baseFolder		=	'admin/features';
	protected $table			=	'features';
	protected $header			=	'admin/header.php';
	protected $footer			=	'admin/footer.php';
	public function __construct() { 
		parent::__construct();       
    	$this->load->model(array('Features_model'));
    	$this->load->helper('url');
    	$this->load->library('session');

        if(empty($this->session->userdata("user_id")))
        {
        	$this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out!"]);
        	redirect('Login/index');
        }
      }
	public function index() {   
	
		$num_rows=$this->db->count_all("$this->table");
     	$this->load->library('pagination');

		$config['base_url'] = base_url().'index.php/Features/index';
		$config['total_rows'] = $num_rows;
		$config['per_page'] = 15;
		
		//$config['use_page_numbers'] = TRUE;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		
		$this->pagination->initialize($config);
     	// ******* pagiantion configrtn ends *** /////////
		
	
	
	            
        $data['records'] = $this->Features_model->getAllData($config['per_page'],$this->uri->segment(3));
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/index",$data);
        $this->load->view("$this->footer");
       
     }
     public function add_view(){
        $this->load->view("$this->header");
        $this->load->view("$this->baseFolder/add");
        $this->load->view("$this->footer");
	 }	
	 public function add()
	 {
	 	$title			= NULL;
     	$description		= NULL;     	
     	$submit 		= NULL;
     	
     	extract($_POST);
     	$params['title']		=	$title;
     	$params['description']	 	=	$description;     
     	
     	//Check whether user upload picture
            $this->load->library('upload');
            $fileUpload=array();$isUpload=FALSE;
            $up_image=array(
            	'upload_path'=>'./uploads/features/',
            	'allowed_types'=>'jpg|jpeg|png|gif',
            	'encrypt_name'=>TRUE
            );
          $this->upload->initialize($up_image);
          if($this->upload->do_upload('image')){
		  	$fileUpload=$this->upload->data();
		  	$isUpload=TRUE;
		  	$params['image']  	=	$fileUpload['file_name'];
		  } 
     	 // echo $fileUpload['file_name'];die; 
     	 
     	if(isset($submit))
     	{		
			$res=$this->Features_model->insertData($params);			
			 if($res)
	         {
	         	/*if($res2) {
					//for mail function
			     	$subject = "Sahara Branch Login Credentials";
			     	$msg = "UserName:".$userName."\n"."Password:".$password;
			     	mail($email,$subject,$msg);
				}
	         	*/
	         	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data added successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to add data!"]);
			 }
		}
		 redirect('Features/index');		
	 }
	 public function edit_view()
	 {
	 	$data['fields']=array(
		'id',
		'title',
		'description'
		);
	 	 $this->load->helper('form'); 
         $tableId = $this->uri->segment('3'); 
         $data['condition'] = array(
         'ID'=>$tableId
         );  
         	    
         $data['results']=$this->Features_model->getUpdateData($data);
         $this->load->view("$this->header");
         $this->load->view("$this->baseFolder/edit",$data);
         $this->load->view("$this->footer");
	 }
	  public function edit()
	 {	 
	 	$editId			= NULL;
	 	$title			= NULL;
     	$description		= NULL;  
     	$submit 		= NULL;
     	
     	extract($_POST);
     	$editId				 	=	$editId;
     	$params['title']		=	$title;
     	$params['description']	 	=	$description;     
     	 
     	if($changeImage=='yes')
     	{
     		$res=$this->Features_model->rowWiseData($editId);
     		if (isset($res))
			{
			        $img= $res->image;
			        $path='base_url()/uploads/features/'.$img;
			       // unlink($path);
			}
			//Check whether user upload picture
            $this->load->library('upload');
            $fileUpload=array();$isUpload=FALSE;
            $up_image=array(
            	'upload_path'=>'./uploads/features',
            	'allowed_types'=>'jpg|jpeg|png|gif',
            	'encrypt_name'=>TRUE
            );
            $this->upload->initialize($up_image);
            if($this->upload->do_upload('image')){
		  	$fileUpload=$this->upload->data();
		  	$isUpload=TRUE;
		    } 
     	 // echo $fileUpload['file_name'];die; 
     	 $params['image']  	=	$fileUpload['file_name'];
		}
     	     	     	
     	if(isset($submit))
     	{			
			$res=$this->Features_model->updateAction($params,$editId);
			 if($res)
	         {
			 	 $this->session->set_flashdata("flash", ["type" => "success", "message" => "Data updated successfully!"]);
			 }
			 else{
			 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to update data!"]);
			 }
		}		
        redirect('Features/index');
	 }
  	public function delete() { 
         $id = $this->uri->segment('3'); 
         $res=$this->Features_model->deleteData($id); 
         if($res)
         {
		 	$this->session->set_flashdata("flash", ["type" => "success", "message" => "Data deleted successfully!"]);
		 }
		 else{
		 	 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Failed to delete data!"]);
		 }
        redirect('Features/index');  		
      }
}
