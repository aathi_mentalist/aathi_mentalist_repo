<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Placement extends CI_Controller {

    public function index()
    {
        $this->load->helper('url');
        $this->load->view('frontend/header');
        $this->load->view('frontend/placement');
        $this->load->view('frontend/footer');
    }
}
