<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    public function __construct() { 
		parent::__construct();       
    	$this->load->model(array('Home_model'));
        $this->load->model(array('Banner_model'));
    	$this->load->model(array('Youtube_model'));
        $this->load->model(array('Questions_model'));
    	$this->load->helper('url');
    	$this->load->library('session');
      }

    public function index()
    {
    	$data['records'] = $this->Home_model->getAllData();
    	$data['banners'] = $this->Banner_model->getFrontendData(); 
        $data['videos']  = $this->Youtube_model->getFrontendData();
        $data['features'] = $this->Home_model->getAllFeatureFrontData();
    	$data['update']  = $this->Home_model->getAllUpdateFrontData();
    	$data['gallery'] = $this->Home_model->getAllGalleryFrontData();
        $data['questions'] = $this->Questions_model->getHomePageQuestionData(3,1);
        $this->load->helper('url');
        $this->load->view('frontend/home',$data);
    }
}
