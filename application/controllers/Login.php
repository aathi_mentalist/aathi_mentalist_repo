<?php
defined('BASEPATH') or exit("No direct script access allowed");
class Login extends CI_Controller{
	protected $header			=	'admin/header.php';
	protected $footer			=	'admin/footer.php';
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Login_model');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->library('form_validation');
	}
	public function index()
	{
		$this->load->view('admin/index');
	}
	public function login()
	{ 
		$this->form_validation->set_rules('userName','Username','required|alpha_numeric');
		$this->form_validation->set_rules('password','Password','required');
		if($this->form_validation->run()== false)
		{
			redirect('index');
		}
		else{
			$userName=$this->input->post('userName');
			$password=$this->input->post('password');//echo $userName.$password;die;
			$id=$this->Login_model->resolve_user_login($userName, $password);
			 if ($id) {                
                    // set session user datas
                    $_SESSION['user_id']      = $id;
                    $_SESSION['username']     = $userName;
                    $_SESSION['logged_in']    = (bool)true;
                    $_SESSION['is_confirmed'] = (bool)true;

                    redirect('Login/adminDashboard');                   

                } else {
						 $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Wrong User Name or password!"]);                   
                    redirect('Login/index');
                }
		}
	}
	public function adminDashboard()
	{
		$this->load->view("$this->header");
		$this->load->view('admin/dashboard/index');
		$this->load->view("$this->footer");
	}
	
	public function changePasswordForm () {
        if (@$_SESSION['logged_in']) {
            $this->load->helper('form');
            $this->load->library('form_validation');
            $this->load->view("$this->header");
            $this->load->view('admin/change-password');
            $this->load->view("$this->footer");
        } else {
            redirect('/');
        }
    }
	
	public function changePassword () {
        $this->load->helper('form');
        $this->load->library('form_validation');

        // set validation rules
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('nPassword', 'New Password', 'required');
        $this->form_validation->set_rules('cPassword', 'Confirm Password', 'required');
        if ($this->form_validation->run() == false) {
            // validation not ok, send validation errors to the view
            $this->load->view("$this->header");
            $this->load->view("admin/change-password");
            $this->load->view("$this->footer");
        } else {
            $currentPassword = $this->input->post('password');
            $newPassword = $this->input->post('nPassword');
            $confirmPassword = $this->input->post('cPassword');
            if ($newPassword != $confirmPassword) {
                $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Password mismatch!"]);
                $this->load->view("$this->header");
            	$this->load->view("admin/change-password");
            	$this->load->view("$this->footer");
            } else {
                $user = $this->Login_model->get_user($_SESSION['user_id']);
                if ($this->Login_model->checkPasswordMatch($currentPassword, $user->password)) {
                    if ($this->Login_model->updatePassword($_SESSION['user_id'], $newPassword)) {
                        redirect(base_url().'index.php/login/logout');
                    } else {
                        $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Something went wrong! Please try again."]);
                        $this->load->view("$this->header");
            			$this->load->view("admin/change-password");
            			$this->load->view("$this->footer");
                    }
                } else {
                    $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Current password is incorrect!"]);
                    $this->load->view("$this->header");
            		$this->load->view("admin/change-password");
            		$this->load->view("$this->footer");
                }
            }
        }
    }
	
	//for logout
	 public function logout() {

        // create the data object
        $data = new stdClass();
        $this->load->helper('form');
        $this->load->library('form_validation');

        if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) {

            // remove session datas
            foreach ($_SESSION as $key => $value) {
                unset($_SESSION[$key]);
            }

            // user logout ok
            $this->load->view('admin/index', $data);

        } else {

            // there user was not logged in, we cannot logged him out,
            // redirect him to site root
            //$this->load->view('index', $data);
            redirect('/');
        }

    }
	
}